// Rest API crud setup
const express = require("express");
const app = express();
const morgan = require("morgan");

app.set("appName", "Express Course");
app.set("port", 3000)
app.set("case sensitive routing", true)

app.use(morgan("dev"));

app.get("/", (req, res) => {
  res.send("Hello World!")
})

app.get("/profile", (req, res) => {
  res.send("Profile page");
});

app.all("/about", (req, res) => {
  res.send("Esto es acerca de.");
});

app.get("/dashboard", (req, res) => {
  res.send("Dashboard page");
});

app.listen(app.get("port"));
console.log(`Server ${app.get("appName")} on port ${app.get("port")}`);
