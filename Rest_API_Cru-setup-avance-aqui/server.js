const express = require("express");
const app = express();
const morgan = require("morgan");

app.use(express.json());

log = console.log;

// array
let products = [
  {
    id: 1,
    name: "laptop",
    price: 3000,
  },
];

app.use(morgan("dev"));

app.get("/products", (req, res) => {
  res.json(products);
});

app.post("/products", (req, res) => {
  const newProduct = { id: products.length + 1, ...req.body };
  products.push(newProduct);
  res.send(newProduct);
});

app.put("/products/:id", (req, res) => {
  const productFound = products.find((p) => p.id === parseInt(req.params.id));
  const newData = req.body

  if (!productFound)
    return res.status(404).json({
      message: "Product not Found",
    });

    products = products.map(p => p.id === parseInt(req.params.id) ? {...p, ...newData} : p)

    res.json({
      message: "Producto actualizado"
    })
});

app.delete("/products/:id", (req, res) => {
  const productFound = products.find(p => p.id === parseInt(req.params.id));

  if (!productFound)
    return res.status(404).json({
      message: "Product not Found",
    });

  products = products.filter((p) => p.id !== parseInt(req.params.id));
  res.sendStatus(204);
});

app.get("/products/:id", (req, res) => {
  log(req.params.id);
  const productFound = products.find((p) => p.id === parseInt(req.params.id));

  if (!productFound)
    return res.status(404).json({
      message: "Product not Found",
    });

  res.json(productFound);
});

app.listen(3000);