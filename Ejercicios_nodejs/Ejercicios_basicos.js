/**
 * ejercicios Basicos con algunos modulos
 */
let path = require("path")
let a = 5;
let b = 10;

if (a < b) {
    console.log(`El Resultado es: ${a * b}`);
} else {
    console.log(`El Resultado es: ${a - b}`);
};

console.log(__dirname);
console.log(__filename);
console.log(path.basename(__filename))